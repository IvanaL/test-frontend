import Vue from 'vue'

import BootstrapVue from 'bootstrap-vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import VueDraggable from 'vuedraggable'

import App from './App.vue'

Vue.use(BootstrapVue)
Vue.use(VueAxios, axios)
Vue.use(VueRouter)
Vue.use(VueDraggable)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import Home from './components/Home.vue';

import { ModalPlugin } from 'bootstrap-vue'
Vue.use(ModalPlugin)

const router = new VueRouter({
  routes: [{
      path: '/',
      name: 'home',
      component: Home
  }]
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
